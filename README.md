# Branches:

## Pattern

```
(master)|(develop)|(release)\/((\d+)\.(\d+)\.(\d+))|(hotfix)|(feature)
```


## The following branches are to be tested and expected to pass:

1. 
```
master
```

2. 
```
develop
```

3. 
```
feature
```

4. 
```
release/0.1.2
```

5. 
```
release/1.3.34
```

6. 
```
release/12.3.0
```

7. 
```
hotfix
```


# Commits:

## Pattern:

```
(fix|feat|style|docs|refactor|test|improvement)(?:\((.+)\))?!?:([\w \d]+)(?:(?:\n)(.*))?
```

## The following commits are to be tested and expected to pass:

1. 
```
docs(lang): Update getting started documentation
```

2. 
```
fix!: User search will ignore null values
```

3. 
```
improvement: Accelerate to 88 miles per hour
Increased speed to a more user friendly value
```

4. 
```
feat: Open the pod bay doors
System can now open doors from the inside
```

5. 
```
style: Refactor subsystem X for readability
Added extensive comments to explain obscure systems
```

6. 
```
docs(lang3.0): Finish portuguese documentation
```

7. 
```
refactor: Remove deprecated methods
```

## The following commits are to be tested and expected to fail:

1. 
```
docs(lang4.0):
Finish finnish documentation
```