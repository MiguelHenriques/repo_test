## Pattern

```
(master)|(develop)|(release)\/(\d+)\.(\d+)\.(\d+)|(hotfix)|(feature)
```


## The following branches are to be tested and expected to pass:


1. 
```
master
```

2. 
```
develop
```

3. 
```
feature
```

4. 
```
release/0.1.2
```

5. 
```
release/1.3.34
```

6. 
```
release/12.3.0
```

7. 
```
hotfix
```